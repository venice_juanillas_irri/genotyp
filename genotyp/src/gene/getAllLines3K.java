package gene;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

/**
 * Servlet implementation class getcountries
 */
@WebServlet("/getAllLines3K")
public class getAllLines3K extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public getAllLines3K() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		String serverName = "172.29.4.215";
		String user = "nicka";
		String pass = "nicka";
		String SID = "orcl";
		String URL = "jdbc:oracle:thin:@" + serverName + ":" + 1521 + ":" + SID;
		Connection conn = null;

		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		boolean is1 = true;
		out.print("{\"lines\":[");
		try {
			conn = DriverManager.getConnection(URL, user, pass);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String SQL = "Select IRIS_UNIQUE_ID, VARNAME_OF_GEN_STOCK_SRC, VARIETYGROUP_OF_SOURCE, COUNTRY_OF_ORIGIN_OF_SOURCE, list_name, is_sequenced, IS_CLUSTERED, box_position_code "
				+ "from list_3k";
		Statement stat = null;
		try {
			stat = conn.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ResultSet rs = null;
		try {
			rs = stat.executeQuery(SQL);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			while (rs.next()) {
				String id = rs.getString(1);
				String nam = JSONObject.escape(rs.getString(2));
				String group = rs.getString(3);
				String country = rs.getString(4);
				String lst = rs.getString(5);
				int is_seq = rs.getInt(6);
				int is_dist = rs.getInt(7);
				String boxpos = rs.getString(8);
				if (!is1)
					out.print(",");
				out.print("[\"<input type=\'radio\' name = \'x\'>\",\"" + id + "\",\"" + nam + "\",\"" + group + "\",\"" + country + "\",\"" + lst + "\","+ is_seq + ","+is_dist+",\""+boxpos+"\"]");
				is1 = false;
			}
			out.print("]");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			stat.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		out.print("}");
		out.flush();
	}

}
