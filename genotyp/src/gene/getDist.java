package gene;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Servlet implementation class getDist
 */
@WebServlet("/getDist")
public class getDist extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public getDist() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    StringBuilder sb = new StringBuilder();
	    BufferedReader br = request.getReader();
	    String str;
	    while( (str = br.readLine()) != null ){
	        sb.append(str);
	    }
	 //   String jsonStr= "{\"countries\":\"Brazi\"}";
	    JSONObject json = null;
	    try {
			json = (JSONObject)new JSONParser().parse(sb.toString());
		} catch (ParseException e) {
			e.printStackTrace();
		}
	    JSONArray countries = (JSONArray) json.get("countries");
	    Iterator<String> iterator = countries.iterator();
	    StringBuilder ctrs = new StringBuilder();
	    boolean is1=true;
	    ctrs.append("(");
	    while (iterator.hasNext()) {
	    if(is1) {ctrs.append("'"+iterator.next()+"'");is1=false;}
	    else ctrs.append(",'"+iterator.next()+"'");
	    }
	    ctrs.append(")");
		try {
			DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
        String serverName="172.29.4.215";
        String user="nicka"; 
        String pass="nicka";
        String SID="orcl";
        String URL="jdbc:oracle:thin:@"+serverName+":"+1521+":"+SID;
        Connection conn=null;
      		try {
      			conn = DriverManager.getConnection(URL, user, pass);
      		} catch (SQLException e1) {
      			// TODO Auto-generated catch block
      			e1.printStackTrace();
      		}
              String SQL="Select g1.nsftv_id||'_'||g1.accession, g2.nsftv_id||'_'||g2.accession, round(1-perc_ident,3) "
              		+ "from distances, germplasm g1, germplasm g2 "
              		+ "where g1.nsftv_id=distances.nsftv_id1 "
              		+ "and g2.nsftv_id=distances.nsftv_id2 "
              		+ "and g1.country in "+ctrs +" and g2.country in "+ctrs;
              Statement stat=null;
      		try {
      			stat = conn.createStatement();
      		} catch (SQLException e) {
      			// TODO Auto-generated catch block
      			e.printStackTrace();
      		}
              ResultSet rs=null;
      		try {
      			rs = stat.executeQuery(SQL);
      		} catch (SQLException e) {
      			// TODO Auto-generated catch block
      			e.printStackTrace();
      		}
      		Map<String,Map<String,Double>> dists = new TreeMap<String,Map<String,Double>>();
      		List<String> lines = new ArrayList<String>();
      		 try {
     			while (rs.next()){
     				String line1=rs.getString(1);
     				String line2=rs.getString(2);
     				Double d = rs.getDouble(3);
     				if(!lines.contains(line1)) lines.add(line1);
     				if(!lines.contains(line2)) lines.add(line2);
     				if(!dists.containsKey(line1)) dists.put(line1, new TreeMap<String,Double>());
     				dists.get(line1).put(line2, d);

     }
     		} catch (SQLException e) {
     			// TODO Auto-generated catch block
     			e.printStackTrace();
     		}
      		//File tmpdir = (File)getServletContext().getAttribute("javax.servlet.context.tempdir");  
      		//File rootdir= (File)getServletContext().getRealPath("/");
      		//File file = new File(getServletContext().getRealPath("/"), "my_dist");  
      		File file = new File("C:/Users/VJuanillas/git2/genotyp/WebContent/my_dist");
      		String path = file.getAbsolutePath();
      		
      		if (!file.exists()) {
				file.createNewFile();
			}
      		int ipath= path.lastIndexOf(File.separator);
      		String dirPath = path.substring(0,ipath);;
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(String.valueOf(lines.size()));
			bw.newLine();
			for(Iterator<String> i = lines.iterator(); i.hasNext(); ) {
				String line = i.next();
				String linep=line;
				linep=linep.replaceAll("\\W", "_");
				if(linep.length()>10) linep=linep.substring(0, 10);
				while(linep.length()<10) linep+=" ";
				bw.write(linep);
				for(Iterator<String> j = lines.iterator(); j.hasNext(); ) {
					String line2 = j.next();
					bw.write(" "+String.valueOf(dists.get(line).get(line2)));
				}
				bw.newLine();
			}
			bw.close();
			fw.close();
			Runtime run = Runtime.getRuntime();  
			//String com= "/bin/sh -c "+tmpdir+"/neighbor < "+tmpdir+"/input";
			//String com= "/bin/ls / >"+tmpdir+"/ls1.out";
			//String com= "/bin/sh -c /bin/ls / > /home/nalexandrov/workspace/genotyp/WebContent/ls.out";
			String com= dirPath+"\\script1.exe";
			response.setContentType("application/json");
			 PrintWriter out = response.getWriter();
			 StringBuilder jsonObject = new StringBuilder("{");
			 int exitVal;
			 boolean is11=true;
			try{
			Process proc = run.exec(com);
			BufferedReader reader =
					new BufferedReader(new InputStreamReader(proc.getInputStream()));
					while ((reader.readLine()) != null) {}
			exitVal = proc.waitFor();
			}
			catch(Exception e) {
				if(!is11) jsonObject.append(",");
				jsonObject.append("\"msg\":\""+e.getMessage()+"\""); 
				is11=false;
			}
			StringBuilder sb2 = new StringBuilder();
			BufferedReader br2 = null;
			File file2 = new File(dirPath+"/outtree");
			FileReader fr = new FileReader(file2.getAbsoluteFile());
			br2 = new BufferedReader(fr);
			String s;
			while ((s = br2.readLine()) != null) {
				sb2.append(s);
			}
			br2.close();
			fr.close();
			if(!is11) jsonObject.append(",");
			jsonObject.append("\"newick\":\""+sb2.toString()+"\"");
			 jsonObject.append("}");
			 out.print(jsonObject.toString());
			 out.flush();
	}

}
