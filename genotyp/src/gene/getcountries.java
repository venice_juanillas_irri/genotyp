package gene;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class getcountries
 */
@WebServlet("/getcountries")
public class getcountries extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public getcountries() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		String serverName = "172.29.4.215";
		String user = "nicka";
		String pass = "nicka";
		String SID = "orcl";
		String URL = "jdbc:oracle:thin:@" + serverName + ":" + 1521 + ":" + SID;
		Connection conn = null;

		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		boolean is1 = true;
		out.print("{\"countries\":[");
		try {
			conn = DriverManager.getConnection(URL, user, pass);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String SQL = "Select country, count(1) from germplasm where country is not null group by country order by 2 desc";
		Statement stat = null;
		try {
			stat = conn.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ResultSet rs = null;
		try {
			rs = stat.executeQuery(SQL);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			while (rs.next()) {
				String country = rs.getString(1);
				int count = rs.getInt(2);
				if (!is1)
					out.print(",");
				out.print("[\"" + country + "\"," + count + "]");
				is1 = false;
			}
			out.print("]");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SQL = "Select trait, count(1) from phenotypes group by trait";
		try {
			rs = stat.executeQuery(SQL);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			out.print(",\"traits\":[");
			is1=true;
			while (rs.next()) {
				String trait = rs.getString(1);
				int count = rs.getInt(2);
				if (!is1)
					out.print(",");
				out.print("[\"" + trait + "\"," + count + "]");
				is1 = false;
			}
			out.print("]");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SQL = "Select COUNTRY_OF_ORIGIN_OF_SOURCE, count(1) from list_3k where COUNTRY_OF_ORIGIN_OF_SOURCE is not null group by COUNTRY_OF_ORIGIN_OF_SOURCE order by 2 desc";
		try {
			rs = stat.executeQuery(SQL);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			out.print(",\"countries3k\":[");
			is1=true;
			while (rs.next()) {
				String country = rs.getString(1);
				int count = rs.getInt(2);
				if (!is1)
					out.print(",");
				out.print("[\"" + country + "\"," + count + "]");
				is1 = false;
			}
			out.print("]");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			stat.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		out.print("}");
		out.flush();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
