package gene;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Servlet implementation class getcountries
 */
@WebServlet("/getDistV3k")
public class getDistV3k extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public getDistV3k() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    StringBuilder sb = new StringBuilder();
	    BufferedReader br = request.getReader();
	    String str;
	    while( (str = br.readLine()) != null ){
	        sb.append(str);
	    }
	 //   String jsonStr= "{\"countries\":\"Brazi\"}";
	    JSONObject json = null;
	    try {
			json = (JSONObject)new JSONParser().parse(sb.toString());
		} catch (ParseException e) {
			e.printStackTrace();
		}
	    JSONArray varieties = (JSONArray) json.get("varieties");
	    Iterator<String> iterator = varieties.iterator();
	    StringBuilder ctrs = new StringBuilder();
	    boolean is1=true;
	    ctrs.append("(");
	    while (iterator.hasNext()) {
	    if(is1) {ctrs.append("'"+iterator.next()+"'");is1=false;}
	    else ctrs.append(",'"+iterator.next()+"'");
	    }
	    ctrs.append(")");
		try {
			DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		String serverName = "172.29.4.215";
		String user = "nicka";
		String pass = "nicka";
		String SID = "orcl";
		String URL = "jdbc:oracle:thin:@" + serverName + ":" + 1521 + ":" + SID;
		Connection conn = null;

		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		is1 = true;
		out.print("{\"lines\":[");
		try {
			conn = DriverManager.getConnection(URL, user, pass);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String SQL = "Select IRIS_UNIQUE_ID, COUNTRY_OF_ORIGIN_OF_SOURCE, is_sequenced, dist "
				+ "from dist_3k, list_3k where nam1 in " + ctrs+" and nam1!=nam2 and dist <.08 and nam2=list_3k.IRIS_UNIQUE_ID order by dist";
		Statement stat = null;
		try {
			stat = conn.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ResultSet rs = null;
		try {
			rs = stat.executeQuery(SQL);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			while (rs.next()) {
				String id = rs.getString(1);
				String country = rs.getString(2);
				int is_seq = rs.getInt(3);
				float dist = rs.getFloat(4);
				if (!is1)
					out.print(",");
				out.print("[\""+id + "\",\"" + country + "\"," + is_seq + ","+dist+"]");
				is1 = false;
			}
			out.print("]");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			stat.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		out.print("}");
		out.flush();
	}

}
