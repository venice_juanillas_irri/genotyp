package gene;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Servlet implementation class gwas
 */
@WebServlet("/gwas")
public class gwas extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public gwas() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    StringBuilder sb = new StringBuilder();
	    BufferedReader br = request.getReader();
	    String str;
	    while( (str = br.readLine()) != null ){
	        sb.append(str);
	    }
	 //   String jsonStr= "{\"countries\":\"Brazi\"}";
	    JSONObject json = null;
	    try {
			json = (JSONObject)new JSONParser().parse(sb.toString());
		} catch (ParseException e) {
			e.printStackTrace();
		}
	    String trait = (String) json.get("trait");
	    String chrom = (String) json.get("chrom");
		try {
			DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
        String serverName="172.29.4.215";
        String user="nicka"; 
        String pass="nicka";
        String SID="orcl";
        String URL="jdbc:oracle:thin:@"+serverName+":"+1521+":"+SID;
        Connection conn=null;
      		try {
      			conn = DriverManager.getConnection(URL, user, pass);
      		} catch (SQLException e1) {
      			// TODO Auto-generated catch block
      			e1.printStackTrace();
      		}
              String SQL="select round(pos/1000000,2), round(-log(10,pval),2) "
              		+ "from ( select /*+ first_rows */ pos, stats_one_way_anova(var1,val,'SIG') pval, count(1) cnt "
              		+ "from snps s, phenotypes p, germplasm l, genotyping g "
              		+ "where 1=1 "
              		+ "and  s.chrom="+chrom
              		+ " and g.snp_id=s.snp_id "
              		+ "and g.nsftv_id = l.nsftv_id "
              		+ "and p.nsftv_id = l.nsftv_id "
              		+ "and trait = '"+trait+"' "
              		+ "and subpopulation = 'TEJ' "
              		+ "group by trait,subpopulation,chrom,pos "
              		+ " ) where pval <0.005 "
              		+ "order by 2 desc ";
              Statement stat=null;
      		try {
      			stat = conn.createStatement();
      		} catch (SQLException e) {
      			// TODO Auto-generated catch block
      			e.printStackTrace();
      		}
              ResultSet rs=null;
      		response.setContentType("application/json");
    		PrintWriter out = response.getWriter();
    		boolean is1 = true;
    		out.print("{\"gwas\":[");
      		try {
      			rs = stat.executeQuery(SQL);
      			
      		} catch (SQLException e) {
      			// TODO Auto-generated catch block
      			e.printStackTrace();
      		}
    		try {
    			while (rs.next()) {
    				float pos=rs.getFloat(1);
          			float pval=rs.getFloat(2);
          			if (!is1) out.print(",");
          			out.print("["+pos+","+pval+"]");
    				is1 = false;
    			}
    			out.print("]");
    		} catch (SQLException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}

    		out.print("}");
    		out.flush();
	}

}
